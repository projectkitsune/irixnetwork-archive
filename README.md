# IRIX Network - Archive #

This is the contents of IRIX Network's archive.irix.cc, including Apocrypha, in a git repo form for people to be able to easily clone and contribute. 

## Exclusions ##
Nekonomicon is removed from this edition of the archive due ot being mastered by forum user Elf at https://gainos.org/~elf/sgi/nekonomicon/

## Special Notes ##
IRIX Network uses NGINX to serve these pages using a basic static file config. 