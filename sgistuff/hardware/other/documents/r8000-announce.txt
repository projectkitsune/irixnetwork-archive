Subject: MIPS Technologies Announces the World's Fastest Supercomputing Microprocessor
Date: 7 Jun 1994 14:43:07 -0500
Message-ID: <2t2ikb$946@dcdmjw.fnal.gov>

[...]
The R8000 chip set consists of a superscalar integer unit, a dual
floating point  unit and two dual  ported tag RAMs. The  chip set
supports up  to 16 megabytes  (MB) of synchronous  data streaming
secondary  cache. The  R8000,  fully binary  compatible with  the
R4000 family, is the first  processor utilizing the advanced MIPS
IV  instruction  set.  The 3.3-volt  microprocessor  features  16
kilobytes (KB) of instruction cache, 16 KB of dual-ported address
and integer  unit data  cache backed  up by  the up  to 16  MB of
secondary cache  and 1 KB  of branch prediction  cache. Together,
the integer unit  and floating point unit consist  of 3.4 million
transisters. Designed  for a 0.5 micron  L-effective 3-metal CMOS
process, the  integer unit  and floating  point unit  die measure
17.2 mm by 17.3 mm.
[...]