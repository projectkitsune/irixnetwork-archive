New System Combines World's Leading Graphics with World's Leading
Supercomputing Performance

MOUNTAIN  VIEW, California  (July 26,  1994) --Silicon  Graphics,
Inc. (NYSE: SGI) today introduced  POWER Onyx(tm), a new graphics
supercomputer based  on the MIPS(r) R8000(tm)  RISC processor-the
world's  fastest supercomputing  microprocessor.  The new  system
provides true supercomputing performance coupled with the world's
leading graphics capabilities to meet  the needs of a diverse and
demanding computing community. Silicon Graphics is also expanding
its high-end graphics family  with an Extreme(tm) Graphics option
and  a unique  feature-known as  the Triple  Keyboard Option-that
allows one system to function as three.

"The  POWER Onyx  system  creates an  entirely  new paradigm  for
affordable  supercomputing andvisualization,"  said Mike  Ramsay,
vice  president  of  Silicon   Graphics'  Visual  Systems  Group.
"Silicon  Graphics'  visualization  leadership now  extends  into
high-performance   supercomputing   environments   and   provides
researchers and scientists with  an affordable, integrated visual
supercomputer."

POWER  ONYX  INCORPORATES  MIPS R8000  PROCESSOR,  SUPERCOMPUTING
PERFORMANCE
The POWER Onyx graphics supercomputer is based on the 64-bit MIPS
RISC   R8000  microprocessor   which   provides  floating   point
computational performance  roughly equivalent to a  Cray Y-MP(tm)
processor. Offering  a record-setting SPECfp92 of  310, the R8000
microprocessor  is the  first superscalar  implementation of  the
MIPS  architecture.  The  R8000  CPU is  designed  for  symmetric
multiprocessing  so  that  multiple  processors  can  be  closely
coupled  within   the  same  computer-providing   customers  with
superior  performance  on  applications traditionally  solved  by
large, expensive supercomputers.


NEW  SYSTEM   TARGETS  DIVERSE  INDUSTRIES   WITH  SUPERCOMPUTING
PERFORMANCE AND WORLD'S FASTEST GRAPHICS
The   POWER    Onyx   graphics   supercomputer    combines   this
supercomputing  performance   with  Silicon   Graphics'  renowned
high-end graphics capabilities and targets customers in a variety
of  industries  such  as   computational  chemistry,  oil  &  gas
research, molecular modeling, global weather modeling, structural
dynamics, fluid dynamics, image processing and animation.

Like  the flagship  Onyx(tm)  system, the  POWER  Onyx system  is
offered  with   Silicon  Graphics'   RealityEngine2(tm)  graphics
subsystems  to   provide  customers  with  the   world's  fastest
interactive and  high-end three dimensional design  and real-time
capabilities.  Based  on  a   scalable  and  expandable  graphics
architecture,  RealityEngine2  graphics  subsystems  contain  1.2
gigaflops of floating point  processing power dedicated solely to
performing geometric transformations. In addition to its advanced
texturing and anti-aliasing capabilities, RealityEngine2 graphics
subsystems  set  new  standards   for  image  quality  and  offer
unprecedented   performance   and    realism   for   complex   3D
environments.

EXTREME GRAPHICS OPTION NOW OFFERED ON POWER ONYX AND ONYX
In addition  to its  RealityEngine2 graphics  subsystems, Silicon
Graphics  will now  provide an  Extreme Graphics  option for  its
POWER Onyx  and Onyx  product line. As  the low-cost  entry point
into the  POWER Onyx and  Onyx family, Extreme  Graphics provides
fast, interactive 3D  graphics and is based on  a single graphics
board design delivering 600K  triangle mesh/second and features a
24-bit hardware Z buffer, 24-bit color, advanced lighting models,
point/line,  anti-aliasing,  atmospheric   effects  and  software
texture mapping.

ONYX EXTENDS CAPABILITIES WITH TRIPLE KEYBOARD OPTION (TKO)
With  the  new Triple  Keyboard  Option  (TKO), Silicon  Graphics
allows Onyx  customers to support  up to three graphics  users on
one workstation.  Designed to increase productivity  and maximize
workstation  resources, the  TKO allows  three separate  graphics
users, with individual  keyboards, monitors and mice,  to share a
single Onyx  system. This option combines  processing, memory and
disk resources, while  simplifying administration, networking and
maintenance functions.

New  or existing  Onyx systems  can be  upgraded to  support this
multiple-user  configuration.  The  TKO   requires  a  Rack  Onyx
workstation  with two  or  three  RealityEngine2 subsystems,  one
POWERChannel-2 I/O subsystem per user, additional monitors, and a
TKO  upgrade kit  which  includes additional  keyboards, 75  foot
monitor and  keyboard cable  extensions, I/O panel  circuitry and
multiuser software.

POWER ONYX RUNS IRIX 6.0-NEW 64-BIT OPERATING SYSTEM
POWER Onyx systems run the IRIX(tm) 6.0 operating system, Silicon
Graphics'  enhanced 64-bit  version  of the  System  V Release  4
(SVR4) UNIX(r)  operating system. IRIX  6.0 takes advantage  of a
multi-threaded symmetric multiprocessing  architecture and offers
advanced   capabilities  to   handle  floating   point  intensive
applications.  Included  are  the new  MIPSpro(tm)  compilers,  a
complete family of optimizing  64-bit compilers providing support
for  Fortran77, C  and C++.  POWER Onyx  also is  augmented by  a
variety of graphics libraries and development environments.

Silicon Graphics  will also offer  a single processor  version of
the POWER Onyx system in a compact deskside enclosure. The Single
Processor POWER  Onyx system is the  world's fastest uniprocessor
graphics  workstation, delivering  up  to 300  peak MFLOPS.  This
system can  be easily upgraded to  a dual-processor configuration
via  a  simple CPU  board  swap  and  is available  with  Extreme
Graphics and RealityEngine2 options.

In  addition to  a single  processor  version of  the POWER  Onyx
system, a large rack chassis enclosure which supports from two to
12  processors  and   delivers  up  to  3.6   gigaflops  of  peak
performance is available. The POWER Onyx system will be available
in August.
