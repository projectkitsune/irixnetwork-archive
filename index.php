<html>
<head>
<title>Archives</title>
<style>
body{
    font-family: Arial, sans-serif;
}
</style>
</head>
<body vlink="blue" background="archive_assets/sgi_tilable_background.gif">
<center>
<table cellspacing="0" cellpadding="0" border="0"><tr>
<td>

<table border="0" width="480">

<tr><td><center><img src="archive_assets/sgistuff.png"></center></td></tr>
<tr><td><b><a href="http://archive.irix.cc/sgistuff">SGIStuff</a></b> - A site featuring information, history, high-resolution photos, tips and
tricks and more for most SGI systems. Created by Gerhard Lenerz between 2001 and 2014, became essentially inaccessible (due to the
server sending an incorrect Content-Type) in early 2018. Also features in-depth pages on software, graphics options, and
Gerhard's computer collection. Includes excellent photos of just about everything it talks about, including parts.</td></tr>

<tr height="30"><!--spacer --></tr>

<tr><td><center><img src="archive_assets/surf.gif"></center></td></tr>
<tr><td><b><a href="http://archive.irix.cc/siliconsurf">Silicon Surf 1994</a></b> - The official Silicon Graphics website, circa 1994. As many users didn't
have access to the internet in 1994, the site was distributed on a CD, which was eventually found and uploaded by Dan Rich to his site at
employees.org/~drich/SGI/SiliconSurf. This version of SiliconSurf predates Wayback Machine archives, and, as such, is the earliest known copy
of SGI's website. It features then-new systems such as the Indy, Indigo2, and Onyx, as well as usual company website information such as news,
technical details, investor information, and more.</td></tr>

<tr height="30"><!--spacer --></tr>

<tr><td><center><img src="archive_assets/forumlogo.gif"></center></td></tr>
<tr><td><b><a href="http://archive.irix.cc/developerforum95">SGI Developer Forum '95</a></b> - Though originally released as web files stored on a 
multimedia CD, SGI's Developer Forum '95 site is just as functional online as it is offline. Also archived by Dan Rich (see above), Developer
Forum '95 is a multimedia CD released by SGI after said developer conference. It contains pictures of the event, details of its happenings,
audio and video recordings of presentations (though some are in IRIX-specific formats), as well as slide decks and a wealth of information useful
to the developers the CD would originally have been given to.</td></tr>

</table></td>
<td width="50"><!-- spacer cell --></td>
<td width="300" valign="top"><h2>
<br /><br /><br /><br /><br />
The SGI Archive</h2>
Information about obscure or old topics (categories most certainly occupied by SGI systems) gets lost easily. SGI has done
little to preserve significant details of their past (we don't blame them, they've got a company to run and/or bankrupt three
consecutive times then sell twice). Throughout the years (all the way back to Jonathan Levine's 1993 IRIS FAQ!) hobbyists have tried
their best to preserve and organize information, however many of the older sites which once held this information are no longer
operational. The SGI Archive is a project of The IRIX Network to re-release SGI-related information which is no longer available
at its original location, or which was never available online. 
<br /><br />
This page only lists content which is no longer available online (or never was available), and has been re-hosted on IRIX Network servers. For currently operational SGI websites, please see our list of <a href="http://irix.cc/sites.php">other sites</a>.
<br /><br />
...or go back to our <a href="http://irix.cc">homepage</a>.
</td>
</tr></table>

</center>
</body>
</html>