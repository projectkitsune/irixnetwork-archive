<title>Technical Paper</title>
<H1>PROJECT HISTORY</H1>

     It is a convenient simplification to describe the history of
the IRIS InSight project as involving four primary activities:
<OL>

     <LI> Project initiation and requirements analysis (late 1990-
       mid 1991)

     <LI> Design and development (mid 1991-late 1992)

     <LI> Process characterization and institutionalization (late
       1991-present)

     <LI> Deployment and enhancement (late 1992-present).
</OL>
<P>


<H2>Project Initiation and Requirements Analysis</H2>

     A project to develop online documentation can be initiated
for numerous reasons, and a successful project begins by
identifying those that are most important and focusing on them.
A major factor in getting the IRIS InSight effort underway in
late 1990 was the success of a new initiative to deliver software
using CD-ROM.  Reducing software distribution costs using CD-ROM
made the cost of shipping printed documentation more visible as a
cost of sales.  We were wary that many people would assume that
delivering documentation online was little more than a change of
distribution medium, and we spent considerable time educating
people about the greater challenge posed by online documentation
[4, 10].
<P>

     We recognized that unless the online documents were highly
usable, Silicon Graphics' customers would refuse to give up
printed documents and there would be little cost savings.
Silicon Graphics was about to launch the Indigo workstation, a
low-end (for Silicon Graphics) computer that has since
substantially enlarged its customer base.  We knew that this
customer mix has much higher expectations for documentation and
ease of use than the engineering and scientific users who
comprise the high-end graphics computing market that Silicon
Graphics had long dominated.  Silicon Graphics couldn't afford an
online documentation system that generated more customer support
calls than it prevented.
<P>

     An additional factor that motivated the IRIS InSight project
was competitive pressure from other workstation vendors.  Other
vendors were already providing online documentation, and this was
beginning to show up in product comparisons, where Silicon
Graphics computers consistently rated outstanding on all factors
except documentation.  We turned being last into an advantage by
conducting a careful competitive analysis and resolving to
"leapfrog" the competition with an aggressive online
documentation project that provided new capabilities to users.
<P>

     Several organizations at Silicon Graphics were considering
online documentation, and it was essential for us to focus these
disparate efforts into a single company-wide initiative.  It was
not difficult to convince the managers of projects for whom
online documentation was a third or fourth priority to defer to a
project for which it was the primary focus, but this consensus-
building and consolidation took several months.
<P>

     In early 1991 we conducted a survey of Silicon Graphics
developers and customers to ask about their plans and
requirements for online document delivery.  The results clearly
spelled out four requirements for Silicon Graphics' effort:
<UL>

    <LI> Don't use proprietary technology

    <LI> Develop a system that also works on other platforms

    <LI> Add additional functionality that takes advantage of
       Silicon Graphics digital media capabilities

    <LI> Use production methods that developers can use as well.
</UL>
<P>


     These results seemed somewhat surprising at first.  Many
people at Silicon Graphics expected that developers would want a
system that competitively exploited Silicon Graphics' superior
digital media technology.  However, since many companies that
develop applications for Silicon Graphics computers also develop
for other Unix computers and the Macintosh, they needed an online
documentation approach that would not be limited to Silicon
Graphics' line.  Developers wanted to be able to create a single
set of documents that could be viewed on all of the computers on
which they sold software.  IRIS InSight would obviously have to
be standards-based.  But which standards?
<P>

<H2>Design and Development</H2>

     Our requirements analysis mandated some kind of standards-
based approach for online documentation, but we were not in any
way predisposed to select SGML.  We were probably predisposed NOT
to use SGML because we knew initially knew much less about it
than the two alternatives we considered.  The first was Adobe's
Postscript, the de facto standard for formatted page description,
already in use by Sun and other vendors as the internal
representation for online documentation,   The second was
Microsoft's RTF (Rich Text Format), the ASCII interchange format
for Word that is widely used in the PC marketplace.
<P>

     We spent several months studying the technical and business
issues and the IRIS InSight engineering team demonstrated that
either Postscript or RTF would satisfy Silicon Graphics'
requirements for delivering online documentation.  But what
finally led us to choose SGML was our competitive analysis of the
online documentation offerings of other workstation vendors and
the survey of Silicon Graphics developers.  Only SGML would
enable the shift from a vendor-centered "here's our online
documentation" strategy to a customer-centered "here's an online
documentation viewer that you can also use" strategy.  The
positive response from the Silicon Graphics developer and
customer base confirms that we made a good decision.
<P>

     Once we chose SGML and began our system design, we had the
luxury of several "build vs. buy" decisions.  Because SGML is a
vendor-independent standard, many companies are building SGML-
based products to support various parts of the SGML publishing
process.  We decided that it would let us ship a product faster
and it would be less proprietary if we relied in part on third-
party tools.  We chose FastTag from Avalanche Development for
format translation and the DynaText toolkit from Electronic Book
Technologies of Providence, Rhode Island to satisfy some of our
indexing and presentation requirements.  Both are technology
leaders in their respective niches.
<P>

     IRIS InSight takes advantage of the unique digital media
capabilities to present video, audio, 3-D graphics, and animation
on Silicon Graphics computers, but multi-platform developers can
use the basic DynaText viewer to deliver some of this information
on other platforms with no changes to the object files.  Without
DynaText, we would be unable to meet the cross-platform
requirement we identified in the 1991 survey of Silicon Graphics
developers.
<P>

     One initial design goal was to maintain fidelity of content
between the printed manuals and those we delivered online.  By
this we mean that we would not require page fidelity, an exact
correspondence between online and printed versions in page
numbering, layout, or formatting, but the structural organization
would be the same [6]. The first version of IRIS InSight that we
started shipping in late 1992 more or less met this goal.
<P>

     But as we have moved more books into IRIS InSight, we have
come to recognize that content fidelity is not entirely
achievable, nor should it be.  We have discovered some structural
or logical aspects of our printed books that we cannot preserve
in the online versions.  For example, we have no easy way other
than screen snaps to duplicate printed graphics that have text
callouts or the sidenotes that appear in many of our end-user
books.
<P>

     On the other hand, once a critical mass of information
entered IRIS InSight, users started to rely on the online version
as the primary one, so complete content fidelity would limit us
from enhancing the online books where it added value.  We
increased the descriptiveness and number of internal headings in
books because of the effective use that the IRIS InSight
structure view makes of them [6].  We have begun to incorporate
3-D graphics, animation, audio, and video into IRIS InSight
books.  We expect that future releases of IRIS InSight will lead
us further from any correspondence with paper documents.
<P>

     The detailed design of IRIS InSight emerged over time as a
result of a thorough review of the literature, a detailed
analysis of design alternatives, and extensive usability testing.
(We highly recommend [8, 9, 11]). We recognized early that
acceptance of IRIS InSight depended on users being willing to
rely on online documents as their primary version.
<P>

     Researchers from the customer research and usability
organization at Silicon Graphics conducted three usability
studies with both experienced Silicon Graphics customers and
Silicon Graphics novices.  The studies were designed to ensure
that IRIS InSight would be preferred by users and that it
wouldn't generate calls to customer support.  These studies
contrasted several specific design alternatives and objectively
measured performance on information retrieval and problem solving
tasks.  The most important of these were the "granularity" or
"node size" into which online books should be organized and the
appropriate behavior of hypertext links; e.g., whether to scroll
within the same window or open a second window.
<P>

     These carefully-controlled experiments led to the final IRIS
InSight design in which books were the most salient online unit
and in which each distinctive link type has a unique behavior.
User performance with the final IRIS InSight design was equal to
that for the printed manuals, a significant achievement given the
limited experience users had with IRIS InSight.  Finally, users
strongly preferred IRIS InSight to the printed manuals.
<P>

<H2>Process Characterization and Institutionalization</H2>

     Just as we knew we couldn't design a product with only vague
requirements for putting documents online, we knew we had to
carry out a systematic "re-engineering" effort to examine the
existing end-to-end publications process.  In part this caused us
to discover problems that we didn't know we had, such as the
extent to which authors often invented ad hoc tags to meet the
visual requirements for camera-ready copy rather than use only
the official templates and tag sets.  Systematizing and
restructuring the publications process was made more difficult by
Silicon Graphics' periodic reliance on contract authors to meet
publications schedules, especially for major product releases,
because contract authors often work from remote locations and are
otherwise removed from the mainstream publications culture.
<P>

     We gradually recognized that SGML was having a profound
institutional impact at Silicon Graphics and was qualitatively
changing the publications process. We were imposing significant
new responsibilities on technical publications personnel.
<P>

     SGML changes the relative importance of formatting and
structure.  When authors are charged with delivering camera-ready
copy, they are tempted to insert formatting tags until the
document looks just right.  A new requirement to deliver valid
SGML files directs an author's attention to more precise
structural tagging that gives customers the advantages of online
presentation but which requires more work .  Authors do value the
flexibility provided by the multimedia capabilities of IRIS
InSight, but only if they are given the time to take advantage of
them.
<P>

     We chose an incremental strategy for moving to SGML that
attempted to minimize the disruption to existing methods and
technology.  Because Silicon Graphics had just made a major
investment in technology and training to standardize technical
publications on FrameMaker, we knew we had to design a
publications process based on format translation to SGML from
FrameMaker rather than adopt SGML-based authoring technology
directly.  This strategy also preserved Silicon Graphics'
investment in using FrameMaker to create hardcopy documents.
<P>

     We brought in outside consultants to conduct document
analyses to help us standardize our use of FrameMaker.  This was
a critical step, because authoring standards are essential if
automated translation to SGML were to work.  We discovered that
although different writing groups at Silicon Graphics had their
own templates, we were able to extract a common document
architecture and encode that in a single document type definition
(DTD).
<P>

     The DTD is the linchpin of an SGML application because it
formally defines the structure of a valid document instance.  It
establishes the target for format translation and is the basis
for the alternative presentation styles in the IRIS InSight
viewer.  But what authors see most directly are the templates in
FrameMaker, and any changes to these templates ripple into the
DTD and the software for format translation and online
presentation.
<P>

     At the time we didn't fully appreciate this central
importance and vulnerability of the DTD in the overall
publications process.  For months several people on the IRIS
InSight team worked on different pieces of the end-to-end
process, and a change that might be an improvement in authoring
templates or other parts viewed in isolation would be a
destabilizing bug to other pieces.  It took several months for us
to stabilize the production process.
<P>

     Training and re-training of authors and production personnel
has been essential. Initially our authors attended courses taught
by popular industry consultants. But as Silicon Graphics has
learned more about SGML and online documentation, there has been
a major shift in training to very specific "here's how we do it
at Silicon Graphics".  Our needs can no longer be met by the
broad and relatively unspecific training available from
outsiders.  Instead, Silicon Graphics authors have translated
what they learned into Silicon Graphics-speak and it now exists
as a detailed publications style guide as an IRIS InSight book.
This book enables authors to become SGML and IRIS InSight-aware
even if their books are not going online right away.  Authors and
production personnel have also instituted one-on-one and group
training in which they teach each other how to write for online
and how to build books successfully.
<P>

     The production process still needs to be more transparent
for authors.  Passage Systems and Silicon Graphics are currently
working on tools for authors and production personnel that will
hide much of the complexity of SGML, especially the nuances of
the format translation and indexing tools.
<P>

<H2>Deployment and Enhancement</H2>

     Bundling IRIS InSight and its core document library for free
as part of system software and charging for printed manuals
overwhelmingly biased the cost-benefit equation in favor of
online documentation.  This economic incentive encourages Silicon
Graphics customers to consider relying on online documentation as
their primary version.
<P>

     We believe it was critical to the success of IRIS InSight
that we focused on issues of putting text online before we moved
to incorporate digital media into IRIS InSight books.  It was
difficult at times to maintain this conservative tack, since
Silicon Graphics computers are unsurpassed in their capabilities
for incorporating audio, video, animation, and 3-D graphics.
Nevertheless, we had seen other online documentation efforts fail
because they were seduced by the excitement and hype about
multimedia, leading to books with "Macbeth multimedia" -- full of
sound and fury, but signifying nothing (our apologies to
Shakespeare) [5].  We knew that IRIS InSight would never be
scaleable, cost-effective, or usable by our authors or customers
if we relied on hand-crafted production techniques of the sort
encouraged by premature experimentation with multimedia [1, 6].
<P>

     Now that we have a robust process for converting print-based
books to IRIS InSight, we can confidently provide the FrameMaker
templates, the DTD, and the associated format translation and
production software to those wanting to use IRIS InSight to
become online publishers on their own.  Since IRIS InSight text
files are completely compatible with those used by DynaText on
other Unix platforms, the Macintosh, or under Microsoft Windows,
a multi-platform strategy for online documentation is completely
viable.  IRIS InSight can use any SGML DTD, so users remain free
to choose another document architecture if they need one for a
specialized application.  However, by providing a core of SGML
technology tailored for IRIS InSight, Silicon Graphics and
Passage Systems have greatly reduced both the cost and the risk
of adopting SGML.
<P>

<A HREF= "tech_InSight3.html"><IMG SRC = "../images/left.button.gif"></A>
<A HREF= "tech_InSight5.html"><IMG SRC = "../images/right.button.gif"></A>

