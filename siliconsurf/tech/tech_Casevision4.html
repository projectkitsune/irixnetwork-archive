<title>Using the CASEVision Product for 
Large-Scale Software 
Development</title>
<H1>APPENDIX 1:How to Build and Modify a 
CASEVision/Tracker Application</H1>

The CASEVision/Tracker product is a highly flexible tool that enables software 
organizations to create their own bug tracking systems. Unlike commercial off-the-shelf 
systems, which are usually limited to a specific methodology and are difficult to modify, 
CASEVision/Tracker systems are built from the ground up (or from the starter examples 
provided) so that they can be tailored to the specific needs of the software group. Thus, 
there is no need for a software organization to change its current method of bug 
reporting. 
<P>
The tool provides a number of facilities for building a bug tracking system:
<UL>
<LI>A special X/Motif graphical user interface (GUI) for accessing and submitting bugs 
in the system
<P>
<LI>A process description language (PDL) for designing bug-related documents in the 
GUI
<P>
<LI>A database manipulation language (DML) for performing operations in the bug 
database, such as examining bugs and generating summaries
<P>
<LI>Two starter examples that demonstrate how to apply the PDL and can be used to 
build new tracking systems
<P>
<LI>Integration with the ClearCase system, enabling bugs to be tied to release versions
<P>
<LI>A customizable on-line help subsystem 
</UL>
The PDL and DML languages are simple to learn so that in-house bug system 
administrators can readily become self-sufficient in building systems without needing 
outside consultants. The bug tracking structure is completely configurable and lets 
administrators implement constraints on bug submittal and state transitions, automatic 
notifications, and report generation. CASEVision/Tracker systems can be implemented 
on a site-wide, local project, or individual basis.<P>

Any software organization large enough to need defined controls over the bug tracking 
process will find the CASEVision/Tracker tool useful. It is particularly appropriate for 
those organizations that need to comply with the 2167A standards.<P>

<H2>CASEVision/Tracker Terminology</H2>

Before we describe the CASEVision/Tracker design process, it's useful to define a few 
terms. A report refers to all the information related to a bug. The program a user runs to 
access this report is an application. A view is a window for inspecting or changing a bug 
report. It contains some or all of the bug information in the report. Views let the 
designer group appropriate information for different types of users on the system. A 
field is a piece of information in a bug report. A transition is an operation performed on 
a bug report through a view. Generally, a transition either changes the state of the bug 
report (bug status) or the values in one or more fields. A rule is a requirement for a 
transition to take place. An action is a result from a transition. A PDL file is a file coded 
in the process description language that contains all the necessary declarations for an 
application. A PDL file has separate sections for declaring fields, transitions, and views.

<H2>Designing and Using a CASEVision/Tracker System</H2>

The designer of the bug tracking system typically goes through the following steps to 
build a bug tracking system for a software group.<P>
<OL>
<LI>Create a PDL file to match the existing bug tracking system.
<P>
The designer must identify all information required for the bug 
reporting system and decide on the view(s) to be provided. If different 
applications are to be run on the same data, then separate PDL files 
must be created.
<P>
<LI>Compile the PDL file.
<P>
<LI>Provide the users access to the new application.
<P>
The users start up the application by invoking the CASEVision/Tracker 
tool with the name of the PDL file.
</UL><P>
After the bug tracking system is up and running, the administrator (or users with 
permission) can perform special queries on the bug database, using DML. DML is a 
high-level query language that uses the following general format:
<P>
<B>select | insert | update</B><I> field1</I><B> from bug where</B><I> field2 = value
</I><P>
There are thus three operators, select, insert, and update, that can operate on 
the specified field field1 for all bug reports meeting the specified condition. Note that 
field2 can be the same as field1.<P>

<H2>PDL File Format</H2>

As in any database system, the designer must first determine what information is used in 
the bug tracking system. Specifically, a CASEVision/Tracker system requires that the 
designer define the following for a bug report:
<UL>
<LI><I>Fields</I> - The information involved with a bug report is displayed as fields in the form. 
Fields are declared as types. The CASEVision/Tracker tool provides a number of 
types; designers can also provide their own.
<P>
<LI><I>Bug report states</I> - A bug report typically goes through a series of states from initial 
filing, through the repair cycle, and final closure. Designers need to formally define 
the set of states for the bug reports in their organization.
<P>
<LI><I>Transitions</I> - These are the operations that can be performed on a bug report, subject 
to defined controls (rules). Transitions can affect the values in the fields and gener-
ally cause a change of the bug report from its current state to a different state. 
<P>
<LI><I>Rules</I> - A rule is a defined constraint for a given transition on a given field. Rules are 
based on the values of selected field variables and can be implemented with boolean 
logic.
</UL><P>
The bug report definitions are entered into a PDL file to be compiled later to produce the 
application. A PDL file has the general format shown in Figure 12.
<P>

<IMG SRC="../images/Case_figure12.gif"><P>
<B>FIGURE 12.</B>	Format for PDL FIle

<H2>Declaring Bug Report Fields</H2>

As shown in the previous figure, the fields are defined in terms of types at the beginning 
of the PDL file. The CASEVision/Tracker tool supplies a number of pre-defined field 
types. The following code segment shows an example of field variable declarations:
<P><PRE>
fields {
 a_Id: int; //alpha bug ID 

Commit_date: short-text; // date
Description: long-text;
Engineers_pri: int;
Newsgroups: short-text; // enum?
Priority: one-of P1, P2, P3, P4, P5, OBSERVE;
Product: short-text; // product
Project: one-of CASE, CASEVision/ClearCase, Coltrane, Explorer,
 NetVisualyzer, IrisVision,
 Aspen, Birch, Cypress, 
 Dogwood ...;
References: short-text;
Resp_engineer: short-text; // list-of person
Subject: short-text;
Submit_date: short-text; // date
Submitter: short-text; // person
Type: one-of BUG, RFE, TAKEN, CLOSED, NOTE ...;
}
</PRE><P>
Each declaration shows the field variable on the left and its type on the right. The pre-
defined short-text type is used for short one-line fields. The description field requires 
more space and uses the long-text type. Variables with a defined set of values use the 
one-of designator followed by the set of values and optionally an ellipsis (...) if the user 
can enter values outside the set. 
<P>

<H2>Declaring Bug Views</H2>

The formats for bug view windows are declared in the views section in the PDL file. 
The designer can declare any number of views to be applied to the same data in the bug 
tracking system. Each view can be broken down into subwindow areas. A subwindow 
can display such window features as a command menu or individual command buttons, 
a matrix of fields, large fields for extensive input, or a bug list display area (not yet 
implemented). The subwindows are then defined feature by feature from left to right. 
<P>
For example, in a matrix of fields, the fields are declared in the row in which they appear 
from left to right. The label for the field is entered first, enclosed in single quotes, 
followed by the name of the field as declared at the beginning of the PDL file. The 
following code segment shows the declaration of a typical bug view. Figure 13 ties the 
code to the resulting view window (Figure 14). 
<P>

<IMG SRC="../images/Case_figure13.gif"><P>
<B>FIGURE 13.</B>	Typical Formatting Declarations in the Views Section of a PDL File

The view uses the label "AlphabugView." The reason for the label is that there can be 
multiple views that access the same bug data. Furthermore, the Views menu in the view 
window uses these labels as selections for displaying the other view windows. 
<P>
The part of the window directly under the menu bar can be used to display information 
identifying the current bug report. To use this feature, the designer enters the 
information inside parentheses following the view label. In the example, the following 
fields are displayed there: the current bug report's ID, type, engineer, product, a space 
for formatting, and the priority. These fields are defined in the fields declarations portion 
of the PDL file.
<P>
Below the identification bar is the transition bar, which contains a menu of operations 
that can be performed on the bug report and the Commit button, which confirms the 
operation.
<P>
Next, the subwindows are defined. In this example, there are two subwindows: 
"Header" for short entries and "Description" for entries of indeterminable length. Note 
the use of commas in the "Header" declaration. Commas used in the row declaration 
define the columns. Where commas are not provided, the fields are displayed, next to 
each other according to an algorithm that attempts a proportionate fit.
<P>

<IMG SRC="../images/Case_figure14.gif"><P>
<IMG SRC="../images/Case_figure14b.gif"><P>
<B>FIGURE 14.</B>	View Declaration Code Segment with Resulting Window and Transitions Menu

<H2>Field Pop-up Menus</H2>

When a user holds down the right mouse button over a field, a menu appears displaying 
the commands, "clear" and "revert" and optionally "values." The "clear" command 
clears the field. The "revert" command reverts to the prior value (if there was one). In 
addition, there can also be a selection called "values" to help users fill in the field. For 
example, if the field type (as it was declared at the beginning of the PDL file) uses the 
"one-of" designator, then the selection "value" appears with a rollover menu containing 
the set of values that were declared. Other field types provide different methods of 
displaying values. Users can select a value from the menu or type directly into the field. 
The pop-up and rollover menu for the "type" field is shown in Figure 15. 
<P>
<IMG SRC="../images/Case_figure15.gif"><P>

<B>FIGURE 15.</B>	Example of Field Information

Type was declared in the PDL file as follows:<P>
<PRE>
Type: one-of BUG, RFE, TAKEN, CLOSED, NOTE ...;
</PRE><P>
The ellipsis (...) at the end means that entries outside of the set of values provided can be 
made.

<H2>Declaring Bug Report Transitions</H2>

Transitions are the operations that can be performed on a bug report. There is additional 
information necessary to define a transition; that is, the designer needs to follow these 
steps:
<UL>

<LI>Enter the name of the transition in the transitions section of the PDL file.
<P>
<LI>Define the change in state (if any) as a result of this transition.
<P>
<LI>Define any rules required for the transition to take place.
<P>
<LI>Define any effects (actions) that result from the operation.
</UL><P>
The following code segment shows how the transition entitled SUBMIT is defined for 
this example:<P>

transitions {
		.
		.
		.
		SUBMIT(=>Open) {
			rules {
				Subject.isSet;
				Type.isSet;
         			Description.isSet;
			}
			actions {
				Submitter.setValue($USER.value);
			}
 		} 
		.
		.
        	.
	}
</PRE><P>
As you can see, the SUBMIT transition is declared in the transitions section of the PDL 
file. Following the declaration of the name, the transition is defined inside parentheses. 
In this case, SUBMIT changes the state of the bug report from null status (it doesn't 
exist yet) to "Open". Following the state transition, the rules, if any, are declared inside 
braces, preceded by the keyword "rules". In this example, SUBMIT requires that a 
condition called "isSet" be TRUE for the Subject, Type, and Description fields. This 
simply means that there must be values in these fields for SUBMIT to take place. 
Finally, the designer defines any actions (effects) that result. The example shows that the 
submitter field is set automatically to the current user.
<P>

<H3>Field Manipulations</H3>

The example in this paper demonstrates a very simple system; the CASEVision/Tracker 
application can be used for more complex systems as well. CASEVision/Tracker 
provides the ability to perform some very powerful field manipulations. For example, if 
the field is of type "person", the CASEVision/Tracker tool can determine the person's 
name, e-mail address, and manager. It can perform numerous tests on a field's value and 
enables arithmetic and Boolean manipulation. With these features, designers can create 
highly sophisticated bug tracking systems with such capabilities as customized 
notifications and integration with configuration management. The starter examples will 
show how to apply these features and will demonstrate useful techniques for creating 
bug systems.
<P>
<IMG SRC="../images/line.gif"><P>
<A HREF="../ss.home.page.html"><IMG SRC="../images/home_icon.gif"></A>
<A HREF="techno_corner.html"><IMG SRC="../images/techno_icon2.gif"></A>
<A HREF="tech_Casevision3.html"><IMG SRC="../images/left.button.gif"></A>
<A HREF="tech_Casevision5.html"><IMG SRC="../images/right.button.gif"></A>
