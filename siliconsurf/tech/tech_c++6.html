<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head><title>ViewKit` Library Technical Report</title>
</head>

<H1>Major ViewKit Elements</H1>
For purposes of discussion, the ViewKit framework can be divided 
into several logical groups of classes. The following sections 
discuss some of these logical groups. 
<P>
<H2>Core Framework Classes</H2>
 
The ViewKit library provides a small set of classes that are 
essential for any application or that provide fundamental support 
for other classes in the framework. The most basic of these classes 
is the VkComponent class. This abstract base class supports the 
concept of a user interface component, as described in the book 
Object-Oriented Programming with C++ and OSF/Motif. (Prentice Hall, 
1992). All user interface classes in the ViewKit library are 
derived from the VkComponent class, which defines the minimum 
protocol for all components.
<P>
The core framework classes also include support for features needed 
by nearly all applications. These classes include VkApp, an 
application class that must be instantiated by every ViewKit 
application, classes that support top-level windows, a collection 
of classes that support menus, and several classes that support 
dialog management. All classes are designed to fully implement as 
many typical features as possible. For example, all top-level 
windows and dialogs handle the window manager quit/close protocol. 
Dialogs are cached to balance memory use and display speed. The 
menu system goes beyond simply constructing menus to support 
dynamically adding, removing, replacing items, and more.
<P>
The collection of classes that make up the core part of the ViewKit 
library are closely integrated and work together to support 
essential features required by most applications as automatically 
as possible. Among the basic services supported by the core ViewKit 
framework are single and multi-level undo, support for performing 
interruptible tasks, and support for an application-level callback 
mechanism that allows C++ classes to dynamically register member 
functions to be invoked by other C++ classes.
<P>
<H2>Components</H2>
In addition to the basic user interface elements supported as part 
of the core framework classes, the ViewKit framework provides an 
assortment of useful interface components that may be useful in 
some situations. These include a graph viewer/editor, an input 
field that supports name expansion, an outliner component for 
displaying and manipulating hierarchical information, and others. 
Some ready-made components supported by the ViewKit library are 
useful in rather specific situations. However, developers are 
encouraged to use the architecture of the ViewKit framework to 
create new components, or extend existing components.
<P>
Figure 1 shows a typical ViewKit high-level component. This 
component is a self-contained graph editor/viewer that can be used 
to browse or create an abstract graph representation consisting of 
a collection of interconnected objects. Nodes in the graph can be 
moved interactively. The controls along the bottom of the component 
allow the graph to be zoomed, the orientation to be changed, and 
other operations to be performed. An overview of the graph can also 
be displayed for large graphs. Although the component described here
represents a complex sub-system, the entire component can be 
included in an application simply by instantiating a single C++ 
class, and using the well-defined interface supported by that class.
<P>
<IMG SRC="../images/c%2b%2b_figure3.gif"><P>
Figure 3. A typical high-level ViewKit component.
<P>
Creating high-level components such as the graph viewer shown here 
promotes consistency throughout a set of applications by providing 
elements that users can learn once and then easily recognize in 
multiple applications.
<P> 
<H2>Convenience Utilities</H2>
The ViewKit framework provides various utility functions and classes
that are used throughout the framework. These utilities, which are 
available to developers as well, include simple functions that make 
it easier to load resources (including automatic type conversion), 
classes that support the use of icons, and other miscellaneous 
utilities. 
<P>
One particularly interesting set of utility classes supports 
operations on groups of widgets. For example, VkRadioGroup allows 
an arbitrary collection of Motif toggle buttons to exhibit radio 
behavior, even if these widgets are in different locations in the 
interface or widget hierarchy. The VkAlignmentGroup class allows 
developers to specify a collection of widgets that should be aligned
to the same width, height, x, or y position.
<P>
<h2>Process management</h2>
The UNIX system, as well as the Xt toolkit on which the Motif 
widget set is based, provides various low-level mechanisms for 
dealing with processes. A typical scenario involves starting an 
application from within an already running application and 
exchanging data between the two applications. The ViewKit library 
provides several C++ classes that make it easier to use UNIX 
processes in such situations. These classes handle the details of 
launching the sub-process, setting up pipes, getting information 
from the sub-process, and handling signals.
<P>
<h2>Help Support</h2>
The ViewKit framework provides complete support for using the SGI 
on-line help system. Most of the time, a programmer who uses the 
ViewKit library does not have to do anything to support help, other 
than writing the content of the documentation. The ViewKit 
framework includes several classes that support on-line help within 
ViewKit applications, but these are seldom used directly by 
programmers. 
<P>
<h2>Network Licensing</h2>
The NetLS licensing system supported on the SGI platform provides 
low-level facilities to support license-protected applications. The 
ViewKit framework makes it easy for programmers to use license 
protection in a way that is consistent with the behavior of other 
SGI applications. To license-protect a ViewKit application, the 
application simply instantiates a VkNLS object, providing some 
simple information about the application in the VkNLS constructor. 
The ViewKit framework automatically checks for license expiration 
at regular intervals, displays various dialogs when error 
situations occur, and closes down applications that are not 
properly licensed.
<P>
<h2>Preference Panels</h2>
Many applications support one or more preference panels that allow 
users to customize the behavior of the application. Such preference 
panels can be tedious to write because they may involve large 
numbers of text input fields, labels, toggle buttons, and so on. 
Users expect preference panels to work in a specific way, as well. 
Usually, users select a number of preferences and then select an 
"Apply" button or an "OK" button to apply all changes at once. 
Users also expect to be able to select "Cancel" and return all 
preferences to their previous state, regardless of how many changes 
the user may have made. Programming this behavior for every 
preference panel can be tedious at best. 
<P>
The ViewKit library supports an easy-to-use collection of classes 
for building preference panels. Rather than dealing directly with 
Motif widgets, their placement, callbacks, and so on, programmers 
who use the ViewKit framework can simply create groups of preference
items. These items maintain their own state, which allows an 
application to simply query each item to see if it has been changed.
Layout is handled automatically, and the ViewKit library provides 
the ability to apply or revert all preferences to their previous 
state.
<P>
<h2>Inter-Application Communication</H2>
Few applications operate in a complete vacuum, and many applications
need to be able to communicate with other applications to work 
effectively. The ViewKit library builds on the ToolTalk` 
inter-application communications service to support simple and 
effective inter-application communication. The ToolTalk system is 
based on a broadcast message model, in which applications send 
requests for services they need, as well as notices of actions they 
perform, to a central messaging service. (See Figure 4.) 
Applications also register with the message service by informing it 
of what types of notices and requests they are interested in. The 
message service routes all messages between interested parties 
without applications needing to be aware of what applications are 
running in the user's environment.
<P>
The ViewKit library supports integration with the ToolTalk service 
in two ways. First, the ViewKit framework supplies a higher-level, 
easy-to-use interface to the ToolTalk library that supports a 
simple callback model, not unlike the Xt callback mechanism. Second,
the ViewKit framework provides various abstract classes that handle 
many of the details of registering with the message service and 
handling messages automatically.
<P> 
The Tooltalk system assumes that messages are handled on an 
application level (and in fact messages are always sent between 
processes). However, the ViewKit library also supports individual 
components that need to encapsulate messaging behavior. For example,
imagine a component that allows the user to display and edit some 
data stored in a file. It is easy to imagine multiple applications 
using this component to browse or edit the same data simultaneously.
Using the ToolTalk communication mechanism, the component can 
transparently coordinate access and display of the data between 
applications. For example, if the user edits the data in one view, 
that view can send notices to other instances of the component to 
keep them in sync. Because this behavior can be encapsulated in a 
component, applications that use the component can benefit with no 
effort on the part of the developer.
<P>
<IMG SRC="../images/c%2b%2b_figure4.gif"><P>
Figure 4. Using the ToolTalk mechanism to communicate between processes.
<P>
<h2>Audio Support</h2>
A user interface is more than images on the screen, and it is 
becoming increasingly common for interfaces to use audio as an 
alternative or a supplement to the visual interface. The ViewKit 
framework provides a straight-forward interface for applications 
that wish to use audio in their applications. In addition, various 
ViewKit components automatically support the use of audio where the 
support can be implemented in a generic manner.
<P>
In some situations, audio can be used to emphasize or complement 
user or system actions. For example, when an error occurs, the 
framework can post an error dialog and also signal the error 
audibly. Audio can also be used to subtly enhance the user 
interface by using audio to smooth transitions and improve the 
user's perception of the application's response time. In many cases,
such features can be provided automatically by the framework. The 
ViewKit framework also helps by providing consistency in the use of 
such techniques among multiple applications.
<P>
Accepting speech input generally requires some effort on the part of
the developer, although a framework can provide support to minimize 
this effort. A framework can also provide automatic speech input 
support in some cases. For example, it is reasonable for a framework
to allow dialogs to be dismissed verbally, with no effort on the 
part of the application developer.
<P>

___________________________________________________________________
<P>
<A HREF = "tech_c%2b%2b5.html"><IMG SRC = "../images/left.button.gif"></A>
<A HREF = "tech_c%2b%2b7.html"><IMG SRC = "../images/right.button.gif"></A>
