<Title>Summary of The Inventor Mentor - Chap 5</Title>


<H1>Chapter 5: Shapes, Properties, and Binding</H1>

This chapter gets into the nitty-gritty of how you create things in
Inventor. It describes the basics of how you create shapes, and
describes how to apply properties to those shapes.


<UL>
<a href="#Shapes"><LI>Shapes</a>
<a href="#Props"><LI>Properties</a>
<a href="#Bind"><LI>Binding</a>
<a href="#Normals"><LI>Normals</a>
<a href="#Trans"><LI>Transformations</a>
</UL>
<P>

<a href="TIMSummary.html">Up to <I>Summary of The Inventor Mentor</I></a><br>
<a href="Chap4.html">Back to <I>Chapter 4 - Cameras and Lights</I></a><br>
<a href="Chap6.html">On to <I>Chapter 6 - Text</I></a><br>

<HR>

<H3><a name="Shapes">Shapes</a></h3>

All shapes are subclasses of the SoShape class. There are two major
categories of shapes: simple and complex. Simple shapes include cubes,
cones, spheres, and cylinders; complex shapes allow you to describe
more complex surfaces in a variety of ways -- face sets, line sets,
triangle strips, point sets, quad meshes, and NURBS.<P>

Face Sets allow you to define a solid shape in terms of the polygons
that make up its faces. You create an array of coordinates containing
all of the vertices of all of the faces, in order. Note that this may
involve duplication of vertices.<P>

An Indexed Face Set is like a Face Set, except that the vertices are
indexed. This allows you to refer to vertices in any order, and
reuse them, thus often saving some space.<P>

A Triangle Strip Set allows you to create a surface out of a
collection of triangles. It works much like a Face Set, but is
somewhat faster to render. (However, it may be more inconvenient for
some shapes.)<P>

A Quad Mesh is similar to a Triangle Strip Set, but uses an array of
quadrilaterals to define the shape instead of triangles.<P>

NURBS are described in <a href="Chap8.html">Chapter 8</a>. 

<H3><a name="Props">Properties</a></H3>

There are a number of different kinds of <I>properties</I> that you can
apply, each with a different node class. Whereas shape nodes tell the
rendering engine to draw the shape in question, property nodes affect
the <i>state</i> of the rendering, changing the way that subsequent shapes
will be drawn. Thus, a property must always come before the shapes it
is to affect.

The following kinds of properties are available:
<UL>
<LI>Materials - affects the color and reflectivity of objects
<LI>Draw Style - tells the renderer how to draw shapes
<LI>Light Model - affects how light is treated in the scene
<LI>Environment - specifies ambient properties like fog and light attenuation
<LI>Shape Hints - permit certain rendering optimizations
<LI>Complexity - allows you to specify the level of detail to be drawn 
<LI>Units - allows you to define your units of measurement
</UL>

An SoMaterial node affects subsequent objects in the scene. It allows
you to specify each object's:

<UL>
<LI> ambient color - response to ambient light
<LI> diffuse color - base color of the object 
<LI> specular color - reflected color
<LI> emissive color - color of radiated light
<LI> shininess
<LI> transparency
</UL>

An SoDrawStyle lets you define whether shapes should be drawn as
filled-in regions (the default), as outlines, as arrays of points, or
left invisible. If you choose points, you can specify the size of
those points; if lines, you can specify the width and pattern of the
lines.<P>

An SoLightModel node allows you to specify whether to use Phong
lighting (that is, use the light sources in the scene), or to just
display each object in its base color. Phong lighting is the default,
and usually looks much more natural.<P>

An SoEnvironment allows you to specify the <i>atmosphere</i> of the
scene. Specifically, you can describe:

<UL>
<LI> the intensity and color of the ambient light in the scene; 
<LI> how rapidly light attenuates away from its sources (which can 
be set to constant, linear, or squared attenutation); 
<LI> the kind of "fog" in the scene (there are several grades of fog - "haze" is light, "fog" is moderate, and "smoke" is quite dense)
<LI> the color of the fog
<LI>the distance at which the fog completely obscures the scene.
</UL>

An SoShapeHints node allows you to specify several characteristics of
the subsequent shapes, which can speed up rendering of the
scene. These hints include:

<UL>
<LI> the order of vertices in the faces of the shape
<LI> whether the shape is solid
<LI> whether all of the faces of the shape are convex
<LI> the "crease angle", which can help in automatically generating normals
</UL>

An SoComplexity node allows you to specify how much you want the
renderer to subdivide subsequent objects into polygons. If you specify
less subdivision, rendering in generally faster. You can tell it:

<UL>
<LI> use fewer polygons for objects that are small on the screen
<LI> just draw bounding boxes
<LI> a hint about how much to subdivide (this is simply an abstract 
number between 0 and 1)
<LI> a hint about how carefully to texture the objects
</UL>

An SoUnits node allows you to specify the units of measurement for all
subsequent shapes in the graph. Almost everything you are likely to
want is available, from kilometers down to angstroms, including normal
English units (eg, feet, miles), and one or two esoteric ones (eg,
nautical miles). Meters are the default unit.

<h3><a name="Bind">Bindings</a></h3>

A single Material is often insufficient for an object, particularly a
complex object. Various parts or faces of the object may call for
different colors or reflectivity. Thus, an SoMaterial node can
actually hold an unlimited number of materials -- each field in the
node can have multiple values. The way that these values are applied
to objects is determined by an SoMaterialBinding node.

You can specify a number of different ways to bind a list of materials
onto an object:

<UL>
<LI> the default, which tries to use the "best" binding (usually an 
"overall" binding)
<LI> use no materials
<LI> an "overall" binding, using the first material for the entire shape
<LI> use one material for each "part" of the shape, where "part" is 
appropriate to the kind of object in question
<LI> use one material per part, with indexing
<LI> use one material per face, optionally with indexing
<LI> use one material per vertex, optionally with indexing
</UL>

If you use indexing, the object in question should use a <i>materialIndex</i>
field to specify which materials are bound to which parts of the
shape. If you bind materials to vertices, it interpolates between
those vertices.

Several color plates are provided to illustrate the way Inventor
handles materials, lights, and bindings.

<h3><a name="Normals">Normals</a></h3>

Normals can be specified explicitly, or generated automatically. You
can specify them explicitly in much the same way that materials are
specified and bound to objects. Most complex objects can compute their
normals automatically, but this process is quite compute-intensive,
and can slow things down.

<h3><a name="Trans">Transformations</a></h3>

Transformations are similar to properties in that they affect the
state of the rendering engine. However, they are cumulative: a
transformation adds to previous transformations, unlike properties,
which replace previous properties in the state.


You can specify all of the usual sorts of transformations in an
SoTransform node. Specifically, you can describe:
<UL>
<LI> translation in space
<LI> rotation
<LI> scaling factor
<LI> scaling rotation (a rotation to apply before scaling)
<LI> the center point of the object for rotation and scaling
</UL>

There are also separate node classes for rotation, translation, and
scaling, which are convenient if you are performing only one
transformation.<P>


<HR>
<P>
<Strong>Author/Summarizer:</Strong><BR><I>Mark Waks</I><P>

<hr>

<a href="TIMSummary.html">Up to <I>Summary of The Inventor Mentor</I></a><br>
<a href="Chap4.html">Back to <I>Chapter 4 - Cameras and Lights</I></a><br>
<a href="Chap6.html">On to <I>Chapter 6 - Text</I></a><br>
