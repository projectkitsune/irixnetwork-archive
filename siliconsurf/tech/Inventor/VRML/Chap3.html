<Title>Summary of The Inventor Mentor - Chap 3</Title>


<H1>Chapter 3: Nodes and Groups</H1>

This chapter explains the concept of a scene graph, and shows how to
build scenes out of groups, properties and shapes. It also describes
the concepts of actions, traversal, and traversal state.<P>

Some notes: The phrase "Inventor supports" is used to indicate the
features that are built-in to Inventor. Programmers can extend the
toolkit to support almost anything. Also, I will be showing the ASCII
file format for the examples in the book, even though the book doesn't
present them this way (it shows examples of building scenes through
C++ code). I've also gone a little beyond just summarizing the book
here, adding my own comments as notes.


<UL>
<a href="#Sgraphs"><LI>Scene Graphs</a>
<a href="#Actions"><LI>What Happens When You Apply an Action to a Node?</a>
<a href="#Wnodes"><LI>What's in a Node?</a>
<a href="#Snodes"><LI>Shape Nodes</a>
<a href="#Pnodes"><LI>Property Nodes</a>
<a href="#Groups"><LI>Groups</a>
<a href="#Separators"><LI>Separators</a>
<a href="#OGroups"><LI>Other Subclasses of SoGroup</a>
<a href="#Shared"><LI>Shared Instancing of Nodes</a>
<a href="#Paths"><LI>Paths</a>
<a href="#Fields"><LI>Fields within a Node</a>
<a href="#References"><LI>References and Deletion</a>
<a href="#Ntypes"><LI>Node Types</a>
<a href="#Naming"><LI>Naming Nodes</a>
<a href="#Glossary"><LI>Chapter 3 Glossary</a>
</UL>
<P>

<a href="TIMSummary.html">Up to <i>Summary of The Inventor Mentor</i></a><br>
<a href="Chap2.html">Back to <i>Chapter 2 - An Inventor Sampler</i></a><br>
<a href="Chap4.html">On to <i>Chapter 4 - Cameras and Lights</i></a><br>

<HR>

<H3><a name="Sgraphs">Scene Graphs</H3>

Inventor programs store their scenes in structures called "scene
graphs". A scene graph is made up of "nodes", which represent 3D
objects that are drawn (shapes), properties of the 3D objects
(properties), nodes that contain other nodes and are used for
hierarchical grouping (groups), and others (cameras, lights, etc).

<H3><a name="Actions">What Happens When You Apply an Action to a Node?</H3></a>

Inventor defines a standard set of "actions" that can be applied to a
scene, such as rendering, getting the world-space bounding box of the
scene, or picking (finding out what objects are underneath the mouse
pointer). Each node implements its own action behavior.

<H3><a name="Wnodes">What's in a Node?</H3></a>

Each node contains one or more pieces of information stored in
"fields". For example, the Sphere node contains only its radius,
stored in its "radius" field. Each field class defines methods to get
and set its values. Side note: well-behaved nodes (all standard
Inventor nodes are well-behaved) use only the contents of their fields
and their position in the scene to determine how they behave when
traversed during an action.

<H3><a name="Snodes">Shape Nodes</H3></a>

Inventor supports the following different types of shapes: 
<UL>
<LI>Cone
<LI>Cube
<LI>Cylinder
<LI>Sphere
<LI>Text2
<LI>Text3
<LI>IndexedFaceSet
<LI>IndexedLineSet
<LI>IndexedTriangleStripSet
<LI>FaceSet
<LI>LineSet
<LI>PointSet
<LI>QuadMesh
<LI>TriangleStripSet
<LI>IndexedNurbsCurve
<LI>IndexedNurbsSurface
<LI>NurbsCurve
<LI>NurbsSurface
</UL>

<H3><a name="Pnodes">Property Nodes</H3></a>

The way shapes are drawn is affected by property nodes in the
scene. Inventor supports the following properties:

<UL>

<LI>BaseColor
<LI>ColorIndex
<LI>Complexity
<LI>Coordinate3
<LI>Coordinate4
<LI>DrawStyle
<LI>Environment
<LI>Font
<LI>LightModel
<LI>Material
<LI>MaterialBinding
<LI>MaterialIndex
<LI>Normal
<LI>NormalBinding
<LI>PackedColor
<LI>PickStyle
<LI>LinearProfile
<LI>NurbsProfile
<LI>ProfileCoordinate2
<LI>ProfileCoordinate3
<LI>ShapeHints
<LI>Texture2
<LI>Texture2Transform
<LI>TextureCoordinate2
<LI>TextureCoordinateBinding
<LI>TextureCoordinateDefault
<LI>TextureCoordinateEnvironment
<LI>TextureCoordinatePlane
<LI>AntiSquish
<LI>MatrixTransform
<LI>ResetTransform
<LI>Rotation
<LI>Pendulum
<LI>Rotor
<LI>RotationXYZ
<LI>Scale
<LI>SurroundScale
<LI>Transform
<LI>Translation
<LI>Shuttle
<LI>Units

</UL>

<H3><a name="Groups">Groups</H3></a>

The order in which the shapes are drawn is determined by group nodes
which contain other nodes known as the groups "children". Inventor
supports the following kinds of group nodes:
<UL>
<LI>Group
<LI>Array
<LI>LevelOfDetail
<LI>MultipleCopy
<LI>PathSwitch
<LI>Separator
<LI>Annotation
<LI>Selection
<LI>Switch
<LI>Blinker
<LI>TransformSeparator
</UL>

Inventor draws the scene graph in a recursive fashion starting by
drawing the first (root) node, then drawing its children (if it is a
group). A property must be drawn before a shape node to affect it.<P>

The simplest kind of group node is "Group", which just draws its
children in order.<P>

<H3><a name="Separators">Separators</H3></a>

Separator nodes are used to isolate parts of the scene from the rest
of the scene. A property node inside a Separator will not affect any
nodes outside the Separator. For example, a robot's head and body
might be specified in the file format like this:<P>

<Pre>
#Inventor V2.0 ascii
Separator {
    Separator { # Body
	Transform { translation 0 3 0 }
	Material { # A bronze color:
	    ambientColor .33 .22 .27
	    diffuseColor .78 .57 .11
	    specularColor .99 .94 .81
	    shininess .28
	}
	Cylinder { radius 2.5 height 6 }
    }
    Separator { # Head
	Transform { translation 0 7.5 0 }
	Material { # A silver color:
	    ambientColor .2 .2 .2
	    diffuseColor .6 .6 .6
	    specularColor .5 .5 .5
	    shininess .5
	}
	Sphere { }
    }
}
</Pre>

The use of Separator nodes keeps the translation of the body from
affecting the translation of the head.

<H3><a name="OGroups">Other Subclasses of SoGroup</H3></a>

Switch is a kind of group with a field that specifies which children
should be drawn. It can be set to draw only one child, to draw no
children, or to draw all of the children (in which case it acts like a
Group node).<P>

LevelOfDetail is a kind of group that draws only one of its children
based on how big the object appears on the screen. It can be used to
draw a simpler version of the object with fewer polygons when the
object is far away.

<H3><a name="Shared">Shared Instancing of Nodes</H3></a>

Adding the same node to more than one group creates a "shared
instance". Instancing is useful for geometry that is shared; only one
copy of the geometry needs to be read in and stored in memory. For
example, we can modify the robot scene graph to add legs, like this:<P>

<Pre>
#Inventor V2.0 ascii
Separator {
    Separator { # Body
	Transform { translation 0 3 0 }
	Material { # A bronze color:
	    ambientColor .33 .22 .27
	    diffuseColor .78 .57 .11
	    specularColor .99 .94 .81
	    shininess .28
	}
	Cylinder { radius 2.5 height 6 }

	Separator { # Left leg
	    Transform { translation 1 -4.25 0 }
	    DEF +0 Group { # Shared leg geometry
		Cube { width 1.2 height 2.2 depth 1.1 }
		Transform { translation 0 -2.25 0 }
		Cube { width 1 height 2.2 depth 1 }
		Transform { translation 0 -2 .5 }
		Cube { width .8 height .8 depth 2 }
	    }
	}
	Separator { # Right leg
	    Transform { translation -1 -4.25 0 }
	    USE +0 # Use the leg geometry again
	}
    }
    Separator { # Head
	Transform { translation 0 7.5 0 }
	Material { # A silver color:
	    ambientColor .2 .2 .2
	    diffuseColor .6 .6 .6
	    specularColor .5 .5 .5
	    shininess .5
	}
	Sphere { }
    }
}
</Pre>

The DEF/USE +0 syntax is used to refer to the same node more than
once; if several nodes were multiply instanced, Inventor would write
out each with a unique number after the '+'.

<H3><a name="Paths">Paths</H3></a>

When programming Inventor, the concept of a "Path" is important. A
path is a chain of nodes, from parent to child, through the scene
graph. Paths are returned from picking and searching; for example, if
the user clicks the mouse on the left foot of the robot, a path from
the root of the scene, through the body separator, the left-leg
separator, through the leg group, and finally down to the last cube in
the leg group would be returned. Paths are necessary because nodes may
be multiply instanced; the cube representing the foot cannot be
returned when the user picks on the left foot, because the same cube
is used for both the left and right feet.

<H3><a name="Fields">Fields within a Node</H3></a>

The next few sections of the book discuss how to set and get the value
of single and multiple-valued fields of various types. Each type of
field has its own set of methods for getting and setting its
value(s). For example, MFFloat, which is a type of field that contains
zero or more floating point values, has methods for setting one value,
for deleting a range of values, for returning the number of values
currently being stored, etc.<P>

Fields have an "ignored" flag that can be set and queried. Fields in
nodes that are marked ignored will have no effect. The ignore flag is
written to file as a "~" character after (or in place of) the field's
value.<P>

Nodes have an "override" flag that can be used to force temporary
changes to the scene. For example, to draw everything as lines a
program might put a
<Pre>
DrawStyle { style LINES }
</Pre>
near the beginning of the scene and set its ignored flag; any
subsequent DrawStyle nodes in the scene will be ignored. The override
flag is not written to (or read from) file.

<H3><a name="References">References and Deletion</H3></a>

Nodes are managed while in memory with a reference counting mechanism
that allows a node to be created, added to a scene, and then
forgotten; the node gets properly deleted when the scene is deleted,
even if it is multiply instanced.

<H3><a name="Ntypes">Node Types</H3></a>

Inventor provides programmers with a run-time type mechanism for its
node, engine, action, detail and event classes, that allows
programmers to find out if an object is of a given type, to find out
what class an object is derived from, etc.

<H3><a name="Naming">Naming Nodes</H3></a>

Nodes can be given names. Once given a name, they can be quickly
looked up by name. Names follow the rules for C++ identifiers
([a-zA-Z_][a-zA-Z0-9_]*
<P>Side note: we will probably relax this to be any sequence of
characters not containing the characters +'"\{} because Inventor 2.0
does not check to see if names are valid, and because some Inventor
customers already have programs that give their nodes names like
"Object1:foo[1,14]"). Names are read and written to files using the
'DEF' keyword; for example, a cube named Joe would be written as:
<Pre>
DEF Joe Cube { }
</Pre>
Side note: if multiply instanced, it might be written as Joe+18, but
its name is still just Joe.<P>

<HR>
<H2><a name="Glossary">Glossary</a></H2>
<DL>

<DT>Action
<DD>An operation on a scene. In Inventor, actions are derived from the
SoAction abstract base class. Actions include the GLRenderAction,
RayPickAction, WriteAction, SearchAction, GetMatrixAction,
HandleEventAction and GetBoundingBoxAction.

<DT>Bounding Box
<DD>An axis-aligned box that is guaranteed to contain some part of the
scene. Inventor uses bounding boxes to optimize certain operations.

<DT>Field
<DD>One or more pieces of data stored in a node or engine.

<DT>Group
<DD>A node that contains other nodes as children.

<DT>Node
<DD>The generic name for any object that is part of the scene, such as
lights, cameras, shapes.

<DT>Path
<DD>A series of nodes, each of which is a parent of the next node in the
series. Paths are returned as the result of pick and search actions.

<DT>Picking
<DD>The process of determining which objects intersect a line shooting
through the scene. Typically, this line is the projection of the 2D
mouse cursor into the 3D scene.

<DT>Property
<DD>A node that affects other nodes in the scene.

<DT>Scene, Scene Graph
<DD>A set of nodes grouped together that represent a virtual environment
or 3D world.

<DT>Shape
<DD>A node that represents a visible object, like a cube, sphere, or set
of polygons.

<DT>Traversal
<DD>The process of allowing each node in the scene to perform some action
(for example, to draw itself).

<DT>Type
<DD>The class of an object; what kind of thing an object is. For
example, all cubes are of type Cube. They are also of type Shape and
Node.

</DL>

<HR>
<P>
<Strong>Author/Summarizer:</Strong><BR><I>Gavin Bell</I><P>

<hr>

<a href="TIMSummary.html">Up to <i>Summary of The Inventor Mentor</i></a><br>
<a href="Chap2.html">Back to <i>Chapter 2 - An Inventor Sampler</i></a><br>
<a href="Chap4.html">On to <i>Chapter 4 - Cameras and Lights</i></a><br>
