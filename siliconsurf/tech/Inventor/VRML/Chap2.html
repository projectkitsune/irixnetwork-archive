<Title>Summary of The Inventor Mentor - Chap 2</Title>


<H1>Chapter 2: An Inventor Sampler</H1>

This chapter introduces Inventor programming and some key Inventor
concepts. Most of the chapter are variations on the "Hello Cone"
program, a simple program that displays a cone on the screen.<P>

<UL>
<a href="#Hello_Cone"><LI>"Hello Cone"</a>
<a href="#Engines"><LI>Using Engines to Make the Cone Spin</a>
<a href="#Trackball"><LI>Adding a Trackball Manipulator</a>
<a href="#Examiner"><LI>Adding the Examiner Viewer</a>
<a href="#Naming"><LI>Naming Conventions, Scene Basics</a>
<a href="#Coordinate"><LI>Coordinate Systems in Inventor</a>
<a href="#Glossary"><LI>Chapter 2 Glossary</a>
</UL>
<P>

<a href="TIMSummary.html">Up to <I>Summary of The Inventor Mentor</I></a><br>
<a href="Chap1.html">Back to <I>Chapter 1 - Overview</I></a><br>
<a href="Chap3.html">On to <I>Chapter 3 - Nodes and Groups</I></a><br>

<HR>

<H3><a name="Hello_Cone">"Hello Cone"</a></H3>

The "Hello Cone" program is a 20-line program that opens an
XtRenderArea in a window, creates a 3D scene consisting of a camera,
light, a red material, adds the scene to the XtRenderArea, and then
enters the Xt main event loop, which takes care of rendering the cone
in response to window system events. Bonus information you won't find
in the book: several people in the Inventor group (myself included)
believe that these examples should have been expressed in the ASCII
file format, instead of being presented as programming
exercises. However, since Inventor is a programming library first and
a file format second, we lost that argument. However, I'll present the
file format for the examples here; if you have an SGI machine, you can
use the '/usr/sbin/ivview' program to view them.<P>

Here is HelloCone:

<Pre>
#Inventor V2.0 ascii
Separator {
    PerspectiveCamera {
	position 0 0 4.18154
	nearDistance 2.44949
	farDistance 5.91359
	focalDistance 4.18154
    }
    DirectionalLight {}
    Material {
	diffuseColor [ 1 0 0 ]
    }
    Cone { }
}
</Pre>

<H3><a name="Engines">Using Engines to Make the Cone Spin</a></H3>

The first variation presented adds an engine to make the cone spin. A
rotation transformation (RotationXYZ) is added to the scene, and an
ElapsedTime engine is connected to the rotation's angle so that the
angle of rotation in radians equals the time in seconds since the
program started (the axis of rotation is set to the X axis). In the
ASCII file format, this scene looks like:

<Pre>
#Inventor V2.0 ascii
Separator {
    PerspectiveCamera {
	position 0 0 4.18154
	nearDistance 2.44949
	farDistance	5.91359
	focalDistance 4.18154
    }
    DirectionalLight {}
    RotationXYZ {
	axis X
	angle = ElapsedTime { }.timeOut
    }
    Material {
	diffuseColor [ 1 0 0 ]
    }
    Cone { }
}
</Pre>


<H3><a name="Trackball">Adding a Trackball Manipulator</a></H3>

Next, a trackball manipulator is used to allow the user to
interactively manipulate the rotation of the cone. This is done by
replacing the RotationXYZ node of the previous example with a
TrackballManip:

<Pre>
#Inventor V2.0 ascii
Separator {
    PerspectiveCamera {
	position 0 0 4.18154
	nearDistance 2.44949
	farDistance	5.91359
	focalDistance 4.18154
    }
    DirectionalLight {}
    TrackballManip {}
    Material {
	diffuseColor [ 1 0 0 ]
    }
    Cone { }
}
</Pre>

<H3><a name="Examiner">Adding the Examiner Viewer</a></H3>

Finally, an XtExaminerViewer is used instead of a simple
XtRenderArea. The XtExaminerViewer adds user interface controls to
allow the user to manipulate their view of the scene; for example, a
zoom slider that controls the field of view (like a zoom lens on a
camera), thumbwheel widgets that control rotation about the screen X
and Y axes, etc. The particular viewing paradigm used (ExaminerViewer,
WalkViewer, FlyViewer, etc) is not considered part of the scene and is
not saved in the file format, so there is no corresponding file format
example for this variant on "Hello Cone".

<H3><a name="Naming">Naming Conventions, Scene Basics</a></H3>

The chapter ends with a description of the conventions used by
Inventor. For example, most Inventor C++ classes are prefixed with
"So", which stands for "Scene Object" (at least, that's the official
explanation-- insiders know that it really stands for "Scenario", the
internal name of the project before it had a name), all enumerated
values are all uppercase, etc. The Inventor library also contains some
useful low level classes that are prefixed with "Sb", such as SbLine,
SbColor and SbRotation.

<H3><a name="Coordinate">Coordinate Systems in Inventor</a></H3>

The convention for the coordinate system used by Inventor is also
described. It is a right-handed coordinate system with +z coming out
of the screen. Angles are specified in radians, and objects may be
defined in their own local coordinate system. The world coordinate
space is the default coordinate space; and object can be transformed
into world coordinates by applying all of the transformation affecting
it. Side note: the book implies that cameras and lights are special
and aren't affected by transformations (that they are defined directly
in world coordinates). That isn't true -- transformations are applied
to cameras and lights, just like other objects.<P>

<HR>

<H2><a name="Glossary">Glossary</a></H2>

<DL>
<DT>Camera
<DD>Defines the eyepoint from which the scene is viewed. The
Inventor SoCamera class is an abstract base class that encapsulates
functionality for the two types of cameras Inventor supports,
SoOrthographicCamera and SoPerspectiveCamera.<P>

<DT>DirectionalLight
<DD>Defines light shining in a given direction. <P>

<DT>ElapsedTime
<DD>An engine that outputs how much time has elapsed since a given time
(by default, how much time has elapsed since it was created).<P>

<DT>Engine
<DD>An object that reads one or more values, performs some operation on
those values, and then writes the results into some part of a
scene. Engines can be to program animations, or to create "smart" 3D
objects with behavior.<P>

<DT>ExaminerViewer
<DD>A component with user interface for interacting with a 3D scene. The
ExaminerViewer uses a "hold an object in your hand and rotate it"
paradigm for examining a 3D scene. It maps mouse gestures to 3D
rotations of the camera, to rotate around an object to look at it from
all sides.<P>

<DT>Light
<DD>An object in the scene that casts light into all or part of the
scene. The Inventor SoLight class is an abstract base class; Inventor
supports Directional, Point, and Spot lights.<P>

<DT>Manipulator
<DD>An object that is part of the scene that responds to input device
events and does something. For example, the TrackballManip maps mouse
events into 3D rotations.<P>

<DT>Material
<DD>An object that specifies how a shape's surface will respond to
light. The lighting model used by Inventor for materials is fairly
simple, and includes diffuse color, ambient color, specular color,
specular exponent, emissive color, and transparency.<P>

<DT>Node 
<DD>The generic name for any object that is part of the scene, such
as lights, cameras, shapes.<P>

<DT>Rotation
<DD>A 3-dimensional orientation. Rotations are generally
specified as an axis to rotation about and an angle of rotation about
that axis.<P>

<DT>Scene, Scene Graph
<DD>A set of nodes grouped together that represent a
virtual environment or 3D world.<P>

<DT>Translation
<DD>A 3-dimensional position, specified by three numbers (X, Y and Z). <P>

<DT>Xt
<DD>The X toolkit. Inventor is separated into two libraries;
libInventor is the core library containing all of the
window-system-independent code. On Unix machines running the X window
system, libInventorXt contains window-system-specific components like
the XtRenderArea or the XtMaterialEditor. Inventor is being ported to
Windows NT, which will have similar (but not identical) components as
libInventorXt.<P>

</DL>
<HR>
<P>
<Strong>Author/Summarizer:</Strong><BR><I>Gavin Bell</I><P>

<hr>

<a href="TIMSummary.html">Up to <I>Summary of The Inventor Mentor</I></a><br>
<a href="Chap1.html">Back to <I>Chapter 1 - Overview</I></a><br>
<a href="Chap3.html">On to <I>Chapter 3 - Nodes and Groups</I></a><br>
