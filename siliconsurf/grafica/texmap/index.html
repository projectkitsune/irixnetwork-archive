<html>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
<title>Texture Mapping as a Fundamental Drawing Primitive</title>
</head>
<body>
<!--no_print--><hr>
<pre><img src=book.gif alt="Picture of Booklet" width=188 height=143><img src=book.gif width=188 height=143><img src=book.gif width=188 height=143></pre>
<h2>Texture Mapping as a Fundamental Drawing Primitive</h2>
<h3>Paul Haeberli and Mark Segal</h3>
<h3>Silicon Graphics Computer Systems</h3>
<h4>June 1993</h4>
<img src=../tribar.gif alt="Horiz Bar" width=561 height=13>

<h3>Abstract</h3>
Texture mapping has traditionally been used to add realism to
computer graphics images.  In recent years, this
technique has moved from the domain of software rendering systems to
that of high performance graphics hardware.
<p>
But texture mapping hardware can be used for many more applications
than simply applying diffuse patterns to polygons.
<p>
We survey applications of texture mapping including simple texture
mapping, projective textures, and image warping.  We then describe
texture mapping techniques for drawing anti-aliased lines, air-brushes,
and anti-aliased text.  Next we show how texture mapping may be used
as a fundamental graphics primitive for volume rendering, environment
mapping, color interpolation, contouring, and many other applications.
<p>
CR Categories and Subject Descriptors:
[Computer Graphics]: Picture/Image Generation;
I.3.7 [Computer Graphics]: Three-Dimensional Graphics and
Realism - <em>color, shading, shadowing, texture-mapping, line drawing, and anti-aliasing</em>

<h3>Introduction</h3>
Texture mapping<cite>[Catmull 74]</cite>,<cite>[Heckbert 86]</cite>
is a powerful technique for adding realism to a
computer-generated scene.
In its basic form,
texture mapping lays an image (the texture) onto an object in a scene.
More general forms of texture mapping generalize the image to other
information;
an &quot;image&quot; of altitudes,
for instance,
can be used to control shading across a surface to achieve such effects
as bump-mapping.
<p>
Because texture mapping is so useful,
it is being provided as a standard rendering technique
both in graphics software interfaces and in computer graphics
hardware<cite>[Hanrahan 90]</cite><cite>[Deering 88]</cite>.
Texture mapping can therefore be used in a scene with only a modest
increase in the complexity of the program that generates that scene,
sometimes with little effect on scene generation time.
The wide availability and high-performance of texture mapping makes it
a desirable rendering technique for achieving a number of effects that
are normally obtained with special purpose drawing hardware.
<p>
After a brief review of the mechanics of texture mapping,
we describe a few of its standard applications.
We go on to describe some novel applications of texture mapping.

<h3>Texture Mapping</h3>
When mapping an image onto an object,
the color of the object at each pixel is modified by a corresponding
color from the image.
In general,
obtaining this color from the image conceptually requires 
several steps <cite>[Heckbert 89]</cite>.
The image is normally stored as a sampled array,
so a continuous image must first be reconstructed from the samples.
Next,
the image must be warped to match any distortion (caused, perhaps,
by perspective) in the projected object being displayed.
Then this warped image is filtered to remove high-frequency
components
that would lead to aliasing in the final step: resampling to obtain the
desired color to apply to the pixel being textured.
<p>
In practice,
the required filtering is approximated by one of several methods.
One of the most popular is <em>mipmapping</em><cite>[Williams 83]</cite>.
Other filtering techniques may also be used <cite>[Crow 84]</cite>.
<p>
There are a number of generalizations to this basic texture mapping scheme.
The image to be mapped need not be two-dimensional;
the sampling and filtering techniques may be applied for both one-
and three-dimensional images <cite>[Peachey 85]</cite>.
In the case of a three-dimensional image,
a two-dimensional slice must be selected to be
mapped onto an object's boundary,
since the result of rendering must be two-dimensional.
The image may not be stored as an array but may be procedurally
generated <cite>[Peachey 85]</cite><cite>[Perlin 85]</cite>.
Finally,
the image may not represent color at all,
but may instead describe transparency or other surface
properties to be used in lighting 
or shading calculations <cite>[Carey 85]</cite>.

<h3>Previous Uses of Texture Mapping</h3>
In basic texture mapping,
an image is applied to a polygon (or some other surface facet) by
assigning texture coordinates to the polygon's vertices.
These coordinates index a texture image,
and are interpolated across the polygon to determine,
at each of the polygon's pixels,
a texture image value.
The result is that some portion of the texture image is mapped onto
the polygon when the polygon is viewed on the screen.
Typical two-dimensional images in this application are
images of bricks or a road surface (in this case the texture image is often
repeated across a polygon);
a three-dimensional image might represent a block of marble from which
objects could be &quot;sculpted.&quot;

<h3>Projective Textures</h3>
A generalization of this technique projects a texture onto surfaces as if
the texture were a projected slide or movie <cite>[Segal 92]</cite>.
In this case the texture coordinates at a vertex
are computed as the result of the projection rather than being assigned
fixed values.
This technique may be used to simulate spotlights
as well as the reprojection of a photograph of an object back onto that
object's geometry.
<p>
Projective textures are also useful for simulating shadows.
In this case,
an image is constructed that represents distances from a light
source to surface points nearest the light source.
This image can be computed by performing Z-buffering from the light's
point of view and then obtaining the resulting Z-buffer.
When the scene is viewed from the eyepoint,
the distance from the light source to each point on a surface is computed
and compared to the corresponding value stored in the texture image.
If the values are (nearly) equal, then the point is not in shadow;
otherwise, it is in shadow.
This technique should not use mipmapping, because filtering must
be applied <em>after</em> the shadow comparison is 
performed <cite>[Reeves 87]</cite>.

<h3>Image Warping</h3>
Image warping may be implemented with texture mapping by defining
a correspondence between a uniform polygonal mesh (representing the original
image) and a warped mesh
(representing the warped image) <cite>[Oka 87]</cite>.
The warp may be affine (to generate rotations, translations, shearings,
and zooms) or higher-order.
The points of the warped mesh are assigned the corresponding texture
coordinates of the uniform mesh,
and the mesh is texture mapped with the original image.
This technique allows for easily-controlled interactive image warping.
The technique can also be used for panning across a large texture
image by using a mesh that indexes only a portion of the entire image.

<h3>Transparency Mapping</h3>
Texture mapping may be used to lay transparent or semi-transparent objects
over a scene by representing transparency values in the texture image as
well as color values.
This technique is useful for simulating clouds <cite>[Gardner 85]</cite> and trees
for example,
by drawing appropriately textured polygons over a background.
The effect is that the background shows through around the edges of the
clouds or branches of the trees.
Texture map filtering applied to the transparency and color values
automatically leads to soft boundaries between the clouds or trees
and the background.

<h3>Surface Trimming</h3>
Finally,
a similar technique may be used to cut holes out of polygons or perform
domain space trimming on curved surfaces <cite>[Burns 92]</cite>.
An image of the domain space trim regions is generated.
As the surface is rendered, its domain space coordinates are used to
reference this image.
The value stored in the image determines whether the corresponding point
on the surface is trimmed or not.

<h3>Additional Texture Mapping Applications</h3>
Texture mapping may be used to render objects that are
usually rendered by other, specialized means.
Since it is becoming widely available,
texture mapping may be a good choice to implement these techniques even
when these graphics primitives can be drawn  using special purpose
methods.
<p>
<img src=space82.gif width=82 height=1>
<img src=tmline.gif alt="Figure 1. Anti-aliased line segments." width=400 height=224>
<p>
One simple use of texture mapping is to draw anti-aliased points of
any width.
In this case the texture image is of a filled circle with a smooth
(anti-aliased) boundary.
When a point is specified,
it's coordinates indicate the center of a square whose width is determined
by the point size.
The texture coordinates at the square's corners are those corresponding
to the corners of the texture image.
This method has the advantage that any point shape may be accommodated
simply by varying the texture image.
<p>
A similar technique can be used to draw anti-aliased,
line segments of any width <cite>[Grossman 90]</cite>.
The texture image is a filtered circle as used above.
Instead of a line segment,
a texture mapped rectangle, whose width is the desired line width,
is drawn centered on and aligned with the line segment.
If line segments with round ends are desired, these can be added
by drawing an additional textured rectangle on each end of the
line segment (Figure 1).

<h3>Air-brushes</h3>
Repeatedly drawing a translucent image on a background can
give the effect of spraying paint onto a canvas.
Drawing an image can be accomplished by drawing a texture mapped polygon.
Any conceivable brush &quot;footprint&quot;, even a multi-colored one,
may be drawn using an appropriate texture image with
red, green, blue, and alpha.  The brush image may also
easily be scaled and rotated (Figure 2).
<p>
<img src=space82.gif width=82 height=1>
<img src=airbrush.gif alt="Figure 2. Painting with texture maps." width=400 height=224>
<p>
<h3>Anti-aliased Text</h3>
If the texture image is an image of a character,
then a polygon textured with that image will show that character
on its face.
If the texture image is partitioned into an array of rectangles,
each of which contains the image of a different character,
then any character may be displayed by drawing a polygon with appropriate
texture coordinates assigned to its vertices.
An advantage of this method is that strings of characters may be arbitrarily
positioned and oriented in three dimensions by appropriately positioning
and orienting the textured polygons.
Character kerning is accomplished simply by positioning the polygons
relative to one another (Figure 3).
<p>
<img src=space82.gif width=82 height=1>
<img src=texmap.gif alt="Figure 3. Anti-aliased text drawing." width=400 height=207>
<p>
Antialiased
characters of any size may be obtained with a single texture map simply
by drawing a polygon of the desired size,
but care must be taken if mipmapping is used.
Normally,
the smallest mipmap is 1 pixel square,
so if all the characters are stored in a single texture map,
the smaller mipmaps will contain a number of characters filtered together.
This will generate undesirable effects when displayed characters are too small.
Thus,
if a single texture image is used for all characters,
then each must be carefully placed in the image,
and mipmaps must stop at the point where the image of a single character
is reduced to 1 pixel on a side.
Alternatively,
each character could be placed in its own (small) texture map.

<h3>Volume Rendering</h3>
There are three ways in which texture mapping may be used to obtain
an image of a solid, translucent object.
The first is to draw slices of the object from back 
to front <cite>[Drebin 88]</cite>.
Each slice is drawn by first generating a texture image of the slice
by sampling the data representing the volume along the plane of the slice,
and then drawing a texture mapped polygon to produce the slice.
Each slice is blended with the previously drawn slices using transparency.
<p>
The second method uses 3D texture mapping <cite>[Drebin 92]</cite>.  In this method,
the volumetric data is copied into the 3D texture image.
Then, slices perpendicular to the viewer are drawn.
Each slice is again a texture mapped polygon,
but this time the texture coordinates at the polygon's vertices
determine a slice through the 3D texture image.
This method requires a 3D texture mapping capability,
but has the advantage that texture memory need be loaded only once
no matter what the viewpoint.
If the data are too numerous to fit in a single 3D image, the
full volume may be rendered in multiple passes,
placing only a portion of the volume data into the texture image on each pass.
<p>
A third way is to use texture mapping to implement &quot;splatting&quot; as
described by <cite>[Westover 90]</cite><cite>[Laur 91]</cite>.

<h3>Movie Display</h3>
Three-dimensional texture images may also be used to display
animated sequences <cite>[Akeley 93]</cite>.
Each frame forms one two-dimensional slice of a three-dimensional
texture.
A frame is displayed by drawing a polygon with texture coordinates
that select the desired slice.  This can be used to smoothly 
interpolate between frames of the stored animation.  Alpha values
may also be associated with each pixel to make animated &quot;sprites&quot;.

<h3>Contouring</h3>
Contour curves drawn on an object can provide valuable information
about the object's geometry.
Such curves may represent height above some plane (as in a topographic
map) that is either fixed or moves with the object <cite>[Sabella 88]</cite>.
Alternatively,
the curves may indicate intrinsic surface properties, such as geodesics
or loci of constant curvature.
<p>
Contouring is achieved with texture mapping by first defining a one-dimensional
texture image that is of constant color except at some spot along its length.
Then,
texture coordinates are computed for vertices of each polygon in the
object to be contoured using a <em>texture coordinate generation function</em>.
This function may calculate the distance of the vertex above some plane
(Figure 4),
or may depend on certain surface properties to produce,
for instance,
a curvature value.
Modular arithmetic is used in texture coordinate interpolation to effectively
cause the single linear texture image to repeat over and over.
The result is lines across the polygons that comprise an object, leading
to contour curves.
<p>
<img src=space82.gif width=82 height=1>
<img src=cont.gif alt="Figure 4. Showing distance from a plane with Contouring." width=400 height=297>
<p>
A two-dimensional (or even three-dimensional) texture image may be used
with two (or three) texture coordinate generation functions to produce
multiple curves,
each representing a different surface characteristic.

<h3>Generalized Projections</h3>
Texture mapping may be used to produce a non-standard projection of 
a three-dimensional scene,
such as a cylindrical or spherical projection <cite>[Greene 86]</cite>.
The technique is similar to image warping.
First,
the scene is rendered six times from a single viewpoint,
but with six distinct viewing directions: forward, backward, up, down,
left, and right.
These six views form a cube enclosing the viewpoint.
The desired projection is formed by projecting the cube of images
onto an array of polygons (Figure 5).
<p>
<img src=space82.gif width=82 height=1>
<img src=fish.gif alt="Figure 5. 360 degree fisheye projection." width=400 height=157>
<p>

<h3>Color Interpolation in non-RGB Spaces</h3>
The texture image may not represent an image at all,
but may instead be thought of as a lookup table.
Intermediate values not represented in the table are obtained
through linear interpolation,
a feature normally provided to handle image filtering.
<p>
One way to use a three-dimensional lookup table is to fill it
with RGB values that correspond to, for instance,
HSV (Hue, Saturation, Value) values.
The H, S, and V values index the three dimensional tables.
By assigning HSV values to the vertices of a polygon
linear color interpolation may be carried out in HSV space
rather than RGB space.
Other color spaces are easily supported.

<h3>Phong Shading</h3>
Phong shading with an infinite light and a local viewer
may be simulated using a 3D texture image as follows.
First,
consider the function of x, y, and z that assigns a brightness
value to coordinates that represent a (not necessarily unit length)
vector.
The vector is the reflection off of the surface of the vector from the
eye to a point on the surface, and is thus a function of the normal
at that point.
The brightness function depends on the location of the light source.
The 3D texture image is a lookup table for the brightness
function given a reflection vector.
Then,
for each polygon in the scene,
the reflection vector is computed at each of the polygon's vertices.
The coordinates of this vector are interpolated across the polygon
and index the brightness function stored in the texture image.
The brightness value so obtained modulates the color of the polygon.
Multiple lights may be obtained by incorporating multiple brightness
functions into the texture image.

<h3>Environment Mapping</h3>
Environment mapping <cite>[Greene 86]</cite>
may be achieved through texture mapping in one of two ways.
The first way requires six texture images,
each corresponding to a face of a cube,
that represent the surrounding environment.
At each vertex of a polygon to be environment mapped,
a reflection vector from the eye off of the surface is computed.
This reflection vector indexes one of the six texture images.
As long as all the vertices of the polygon generate reflections into the
same image,
the image is mapped onto the polygon using projective texturing.
If a polygon has reflections into more than one face of the cube,
then the polygon is subdivided into pieces,
each of which generates reflections into only one face.
Because a reflection vector is not computed at each pixel,
this method is not exact,
but the results are quite convincing when the polygons are small.
<p>
The second method is to generate a single texture image 
of a perfectly reflecting sphere in the environment.
This image consists of a circle representing the hemisphere of the
environment behind the viewer,
surrounded by an annulus representing the hemisphere in front of the viewer.
The image is that of a perfectly reflecting
sphere located in the environment when the viewer is infinitely far from the
sphere.
At each polygon vertex,
a texture coordinate generation function generates coordinates that
index this texture image, and these are interpolated across the polygon.
If the (normalized) reflection vector at a vertex
is r = vect(x y z),
and m = sqrt(2*(z+1)),
then the generated coordinates are x/m and y/m 
when the texture image is indexed by coordinates ranging from -1 to 1.
(The calculation is diagrammed in Figure 6).
<p>
<img src=space82.gif width=82 height=1>
<img src=sphmap.gif alt="Figure 6. Spherical reflection geometry." width=385 height=328>
<p>
This method has the disadvantage that the texture image must be recomputed
whenever the view direction changes,
but requires only a single texture image with no special polygon subdivision
(Figure 7).
<p>
<img src=space82.gif width=82 height=1>
<img src=envmap.gif alt="Figure 7. Environment Mapping." width=400 height=198>
<p>

<h3>3D Halftoning</h3>
Normal halftoned images are created by thresholding a
source image with a halftone screen.  Usually this
halftone pattern of lines or dots bears no direct
relationship to the geometry of the scene.
Texture mapping allows halftone patterns to be generated using a 3D spatial
function or parametric lines of a surface (Figure 8).
This permits us to
make halftone patterns that are attached to the surface 
geometry <cite>[Saito 90]</cite>.
<p>
<img src=param.gif alt="Figure 8. 3D halftoning." width=560 height=248>
<p>

<h3>Conclusion</h3>
Many graphics systems now provide hardware that supports
texture mapping.  As a result, generating a texture mapped
scene need not take longer than generating a scene without
texture mapping.
<p>
We hope to have shown that,
in addition to its standard uses,
texture mapping can be used for a large number of interesting
applications, and that texture mapping is a powerful and
flexible low level graphics drawing primitive.

<h3>References</h3>

<p>
[Akeley 93] Kurt Akeley,
<cite>Personal Communication</cite>, 1993
<p>
[Bishop 86] G. Bishop and D. M. Weimer,
<cite>Fast Phong Shading</cite>,
Computer Graphics (SIGGRAPH '86 Proceedings), Pages 103-106, August, 1986.
<p>
[Burns 92] Derrick Burns,
<cite>Personal Communication</cite>, 1992.
<p>
[Carey 85] Richard J. Carey and Donald P. Greenberg,
<cite>Textures for Realistic Image Synthesis</cite>,
Computer and Graphics, Pages 125-138, Vol. 9, No.3, 1985.
<p>
[Catmull 74] Ed Catmull,
<cite>A Subdivision Algorithm for Computer Display of Curved Surfaces</cite>,
PhD Thesis, University of Utah, 1974.
<p>
[Crow 84] Frank Crow,
<cite>Summed-Area Tables for Texture Mapping</cite>,
Computer Graphics (SIGGRAPH '84 Proceedings), Pages 207-212, July, 1984.
<p>
[Deering 88] Michael Deering and Stephanie Winner and Bic Schediwy and
   Chris Duffy and Neil Hunt,
<cite>The Triangle Processor and Normal Vector Shader: A VLSI
          System for High Performance Graphics</cite>,
Computer Graphics (SIGGRAPH '88 Proceedings), Pages 21-30, August, 1988.
<p>
[Drebin 88] Robert A. Drebin and Loren Carpenter and Pat Hanrahan,
<cite>Volume Rendering</cite>,
Computer Graphics (SIGGRAPH '88 Proceedings), Pages 65-74, August, 1988.
<p>
[Drebin 92] Robert A. Drebin,
<cite>Personal Communication</cite>, 1992.
<p>
[Gardner 85] Geoffrey. Y. Gardner,
<cite>Visual Simulation of Clouds</cite>,
Computer Graphics (SIGGRAPH '85 Proceedings), Pages 297-303, July, 1985.
<p>
[Greene 86] Ned Greene,
<cite>Applications of World Projections</cite>,
Proceedings of Graphics Interface '86, Pages 108-114, May, 1986.
<p>
[Grossman 90] Mark Grossman,
<cite>Personal Communication</cite>, 1990.
<p>
[Hanrahan 90] Pat Hanrahan and Jim Lawson,
<cite>A Language for Shading and Lighting Calculations</cite>,
Computer Graphics (SIGGRAPH '90 Proceedings), Pages 289-298, August, 1990.
<p>
[Heckbert 86] Paul S. Heckbert,
<cite>Survey of Texture Mapping</cite>,
IEEE Computer Graphics and Applications, Pages 56-67, November, 1986.
<p>
[Heckbert 89] Paul S. Heckbert,
<cite>Fundamentals of Texture Mapping and Image Warping</cite>,
M.Sc. Thesis, Department of Electrical Engineering and Computer Science,
University of California, Berkeley, June, 1989.
<p>
[Laur 91] David Laur and Pat Hanrahan,
<cite>Hierarchical splatting: A progressive refinement algorithm for volume rendering</cite>,
Computer Graphics (SIGGRAPH '91 Proceedings), Pages 285-288, July, 1991.
<p>
[Oka 87] Masaaki Oka and Kyoya Tsutsui and Akio Ohba and Yoshitaka,
<cite>Real-Time Manipulation of Texture-Mapped Surfaces</cite>,
Computer Graphics (SIGGRAPH '87 Proceedings), July, 1987.
<p>
[Peachy 85] Darwyn R. Peachey,
<cite>Solid Texturing of Complex Surfaces</cite>,
Computer Graphics (SIGGRAPH '85 Proceedings), Pages 279-286, July, 1985.
<p>
[Perlin 85] Ken. Perlin,
<cite>An Image Synthesizer</cite>,
Computer Graphics (SIGGRAPH '85 Proceedings), Pages 287-296, July, 1985.
<p>
[Reeves 87] William Reeves and David Salesin and Rob Cook,
<cite>Rendering Antialiased Shadows with Depth Maps</cite>, 
Computer Graphics (SIGGRAPH '87 Proceedings), Pages 283-291, July, 1987.
<p>
[Sabella 88] Paolo Sabella,
<cite>A Rendering Algorithm for Visualizing 3D Scalar Fields</cite>,
Computer Graphics (SIGGRAPH '88 Proceedings), Pages 51-58, August, 1988.
<p>
[Saito 90] Takafumi Saito and Tokiichiro Takahashi,
<cite>Comprehensible Rendering of 3-D Shapes</cite>,
Computer Graphics (SIGGRAPH '90 Proceedings), Pages 197-206, August, 1990.
<p>
[Segal 92] Mark Segal and Carl Korobkin and Rolf van Widenfelt and Jim
           Foran and Paul Haeberli,
<cite>Fast Shadows and Lighting Effects using Texture Mapping</cite>,
Computer Graphics (SIGGRAPH '92 Proceedings), Pages 249-252, July, 1992.
<p>
[Westover 90] Lee Westover,
<cite>Footprint Evaluation for Volume Rendering</cite>,
Computer Graphics (SIGGRAPH '90 Proceedings), Pages 367-376, August, 1990.
<p>
[Whitted 83] Turner Whitted, 
<cite>Anti-Aliased Line Drawing Using Brush Extrusion</cite>,
Computer Graphics (SIGGRAPH '83 Proceedings), Pages 151-156, July, 1983.
<p>
[Williams 83] Lance Williams, 
<cite>Pyramidal Parametrics</cite>, 
Computer Graphics (SIGGRAPH '83 Proceedings), Pages 1-11, July, 1983.
<hr>
Appeared in the proceedings of the Fourth Eurographics Workshop
on Rendering, Michael Cohen, Claude Puech, Francois Sillion, Editors.
Paris, France, June, 1993.
<hr>
<!--no_print--><i>This is a publication of <a href="../index-2.html"> GRAFICA Obscura</a>.</i>
<!--no_print--><address>paul@sgi.com<br>segal@sgi.com</address>
</body>
</html>
